class ResApi{

    static statusCodes = {
        INTERNAL_SERVER_ERROR: 500,
        BAD_REQUEST:400,
        UNAUTHORIZED: 401,
        FORBIDDEN: 403,
        NOT_FOUND: 404,
        CONFLICT: 409,
        UNPROCESSABLE_ENTITY: 422,
        SUCCESS: 200,
        CREATED: 201
      };
      
    static contentTypes = {
        JSON: 'application/json',
        HTML: 'text/html'
      };
      
    static errors = {
        internalServerError: {
            code: 'internalServerError',
            message: 'Internal server error!'
        },
        routeNotFound: {
            code: 'routeNotFound',
            message: 'Requested route/method not found!'
        },        
    };

    static internalServerError(response,
        body = {
            code: ResApi.errors.internalServerError.code,
            message: ResApi.errors.internalServerError.message
        }) {
        response.writeHead(ResApi.statusCodes.INTERNAL_SERVER_ERROR, {
            'Content-Type': ResApi.contentTypes.JSON
        }).end(JSON.stringify(body));
    }

    static badRequest(response,body = {message:"Bad request" } ){
        response.writeHead(ResApi.statusCodes.BAD_REQUEST, {
                'Content-Type': ResApi.contentTypes.JSON
            }).end(JSON.stringify(body));
    }

    static notAuth(response,body = {message:"Unauthorized" } ){
        response.writeHead(ResApi.statusCodes.UNAUTHORIZED, {
                'Content-Type': ResApi.contentTypes.JSON
            }).end(JSON.stringify(body));
    }

    static forbidden(response,body = {message:"Forbidden" } ){
        response.writeHead(ResApi.statusCodes.FORBIDDEN, {
                'Content-Type': ResApi.contentTypes.JSON
            }).end(JSON.stringify(body));
    }

    static notFound(response,
        body = {
            code: ResApi.errors.routeNotFound.code,
            message: ResApi.errors.routeNotFound.message
        }) {
        response.writeHead(ResApi.statusCodes.NOT_FOUND, {
            'Content-Type': ResApi.contentTypes.JSON
        }).end(JSON.stringify(body));
    }

    static conflict(res,body = {message: "Conflict"}){
        response.writeHead(ResApi.statusCodes.CONFLICT, {
            'Content-Type': ResApi.contentTypes.JSON
        }).end(JSON.stringify(body));
    }
    
    static successful(response,body = {message:"OK" } ){
        response.writeHead(ResApi.statusCodes.SUCCESS, {
            'Content-Type': ResApi.contentTypes.JSON
        }).end(JSON.stringify(body));
    }

    static successfulCreation(response,body = {message:"Created successfully"} ){
        response.writeHead(ResApi.statusCodes.CREATED, {
            'Content-Type': ResApi.contentTypes.JSON
        }).end(JSON.stringify(body));
    }

}

module.exports = ResApi;